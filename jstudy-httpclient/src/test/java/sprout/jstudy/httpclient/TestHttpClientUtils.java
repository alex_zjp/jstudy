package sprout.jstudy.httpclient;

import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: sprout
 * Date: 12-9-19
 * Time: 下午2:55
 * To change this template use File | Settings | File Templates.
 */
public class TestHttpClientUtils {

    @Test
    public void testGet(){
        String url = "http://www.360buy.com";
        try {
            String result = HttpClientUtils.get(url);
            System.out.println(result);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (URISyntaxException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    @Test
    public void testPost(){
        String url = "http://www.baidu.com";
        try {
            String result = HttpClientUtils.post(url);
            System.out.println(result);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (URISyntaxException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    @Test
    public void testGetOfSSL(){
        String url = "https://dynamic.12306.cn/otsweb/loginAction.do?method=login";
        try {
            String result = HttpClientUtils.getOfSSL(url);
            System.out.println(result);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (URISyntaxException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (KeyManagementException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    @Test
    public void testPostOfSSL(){
        String url = "https://dynamic.12306.cn/otsweb/loginAction.do?method=login";
        try {
            Map<String, String> params = new HashMap<String, String>();
//            params.put("loginUser.user_name", "");
//            params.put("user.password", "");
//            params.put("randCode", "");
            String result = HttpClientUtils.postOfSSL(url, params);
            System.out.println(result);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (URISyntaxException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (KeyManagementException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    @Test
    public void testGetForFile(){
        String url = "http://caipiao.360buy.com/lottery_dlt.html";
        String path = "D:/lottery_dlt.html";
        try {
            HttpClientUtils.getForFile(url, path);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (URISyntaxException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    @Test
    public void testGetOfSSLForFile(){
        String url = "https://dynamic.12306.cn/otsweb/passCodeAction.do?rand=sjrand&" + Math.random();
        String path = "D:/12306.jpg";
        try {
            HttpClientUtils.getOfSSLForFile(url, path);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (KeyManagementException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (URISyntaxException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

}
