package sprout.jstudy.ssi.dao.user;

import org.junit.Assert;
import org.junit.Test;
import sprout.jstudy.ssi.BaseTest;
import sprout.jstudy.ssi.domain.enums.SexEnum;
import sprout.jstudy.ssi.domain.user.User;

/**
 * Created by IntelliJ IDEA.
 * User: sjx
 * Date: 13-5-16
 * Time: 下午4:31
 * To change this template use File | Settings | File Templates.
 */
public class TestUserDao extends BaseTest {

    private UserDao userDao;

    @Test
    public void add() {
        User user = new User();
        user.setName("张三");
        user.setSex(SexEnum.MAN.getKey());
        user.setAge(22);
        user.setEmail("zhangsan@jstudy.com");
        boolean added = userDao.add(user);
        Assert.assertTrue(added);
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
}
