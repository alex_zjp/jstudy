package sprout.jstudy.json;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * JSON解析工具类
 * 对json的格式要求比较严格
 * Created with IntelliJ IDEA.
 * User: sprout
 * Date: 13-4-27
 * Time: 下午10:37
 * To change this template use File | Settings | File Templates.
 */
public class JSONUtils {

    //可重用、可共享，只需要一个即可
    private final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static <T> T parser(String json, Class<T> clazz) throws IOException {
        return OBJECT_MAPPER.readValue(json, clazz);
    }

}
