package sprout.jstudy.json;

import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: sjx
 * Date: 13-5-7
 * Time: 下午12:42
 * To change this template use File | Settings | File Templates.
 */
public class TestJSONUtils {

    @Test
    public void testParseJson(){
        //使用双引号，对格式要求比较严格
        String json = "{\"name\" : \"张三\", \"sex\" : \"男\", \"age\" : 25}";
        try {
            HashMap<String, Object> map = JSONUtils.parser(json, HashMap.class);
            for (Map.Entry<String, Object> entry : map.entrySet()){
                System.out.println(entry.getKey() + ":" + entry.getValue());
            }
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

}
