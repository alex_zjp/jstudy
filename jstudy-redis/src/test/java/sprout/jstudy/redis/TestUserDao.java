package sprout.jstudy.redis;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import sprout.jstudy.redis.dao.UserDao;
import sprout.jstudy.redis.domain.User;

/**
 * Created with IntelliJ IDEA.
 * User: shaojiaxin
 * Date: 14-1-10
 * Time: 下午5:30
 * To change this template use File | Settings | File Templates.
 */
public class TestUserDao {

    private UserDao userDao;

    @Test
    public void add() {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        UserDao userDao = (UserDao) context.getBean("userDao");
        User user = new User();
        user.setId(1);
        user.setName("张三");
        System.out.println(userDao.add(user));
    }

    @Test
    public void update() {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        UserDao userDao = (UserDao) context.getBean("userDao");
        User user = new User();
        user.setId(1);
        user.setName("李四");
        System.out.println(userDao.update(user));
    }

    @Test
    public void get() {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        UserDao userDao = (UserDao) context.getBean("userDao");
        User user = userDao.getById(1);
        System.out.println(user);
    }

    public UserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
}
