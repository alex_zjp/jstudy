package sprout.jstudy.mongodb.dao;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;

/**
 * mongo操作
 */
public class MongoDAO {

	private DB db;

    /**
     * 构建mongodb
     * @param address
     * @param port
     * @param dbName
     * @throws Throwable
     */
	public MongoDAO(String address, int port, String dbName) throws Throwable{
	    this.db = new Mongo(address, port).getDB(dbName);
	}

    public DBCollection getCollection(CollectionType collectionType) {
        return db.getCollection(collectionType.getName());
    }

	public void clear(CollectionType collectionType) {
		getCollection(collectionType).drop();
	}

    /**
     * 连接枚举
     */
    public static enum CollectionType{

        USER("user");
        
        private String name;

        private CollectionType(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

}