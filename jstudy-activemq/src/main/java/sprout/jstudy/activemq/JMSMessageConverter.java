package sprout.jstudy.activemq;

import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.jms.support.converter.MessageConverter;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import java.io.Serializable;

/**
 * JMS消息转换
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 13-6-2
 * Time: 下午6:16
 * To change this template use File | Settings | File Templates.
 */
public class JMSMessageConverter implements MessageConverter {

    @Override
    public Message toMessage(Object message, Session session) throws JMSException, MessageConversionException {
        ObjectMessage objectMessage = session.createObjectMessage();
        objectMessage.setObject((Serializable)message);
        return objectMessage;
    }

    @Override
    public Object fromMessage(Message message) throws JMSException, MessageConversionException {
        return ((ObjectMessage)message).getObject();
    }

}
