package sprout.jstudy.activemq;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import sprout.jstudy.activemq.client.MQSender;
import sprout.jstudy.activemq.server.MQReceiver;

import java.util.AbstractMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 13-6-2
 * Time: 下午7:31
 * To change this template use File | Settings | File Templates.
 */
public class TestMQ {

    /**
     * MQ服务端测试，启动服务端监听
     * 1.先去http://activemq.apache.org/download.html下载MQ
     * 2.启动activemq/bin/win32/activemq.bat
     * 3.浏览器访问http://localhost:8161地址，账号密码都是admin
     * 4.访问成功打开activemq管理页面
     * 5.执行本测试方法-服务端监听
     */
    @Test
    public void testServer(){
        try {
            ApplicationContext context = new ClassPathXmlApplicationContext("spring-jms-server.xml");
            MQReceiver mqReceiver = (MQReceiver) context.getBean("mqReceiver");
            mqReceiver.listener();
            Thread.sleep(Integer.MAX_VALUE);
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    /**
     * MQ客户端测试，发送消息到MQ服务端
     */
    @Test
    public void testClient(){
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-jms-client.xml");
        MQSender mqSender = (MQSender) context.getBean("mqSender");

        User user = new User();
        user.setId(1);
        user.setName("张三");
        user.setAge(25);
        user.setEmail("zhangsan@sprout.com");
        user.setAddress("北京昌平区");

        Map.Entry<String, Object> entry = new AbstractMap.SimpleEntry<String, Object>("user", user);
        mqSender.send(MQType.USER, entry);
    }

}
