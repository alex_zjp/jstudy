package sprout.jstudy.reflect;

/**
 * Created by IntelliJ IDEA.
 * User: sprout
 * Date: 12-8-10
 * Time: 下午4:50
 * To change this template use File | Settings | File Templates.
 */
public class Person2 implements China{

    private String name;
    private int age;

    public Person2() {
    }

    public Person2(String name) {
        this.name = name;
    }

    public Person2(int age) {
        this.age = age;
    }

    public Person2(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person2{" +
                "name='" + name + '\'' +
                ", age='" + age + '\'' +
                '}';
    }

    @Override
    public void sayChina() {
        System.out.println("hello ,china");
    }

    @Override
    public void sayHello(String name, int age) {
        System.out.println("hello " + name + "  "+age);
    }
}
